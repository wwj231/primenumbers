# Liczby pierwsze

Liczba pierwsza – liczba naturalna większa od 1, która ma dokładnie dwa dzielniki naturalne: jedynkę i siebie samą.
np.: 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41.

## Zadanie

Zaimplementuj metodę generatePrimeNumbers tak, aby zwracała listę liczb pierwszych z podanego przedziału.
