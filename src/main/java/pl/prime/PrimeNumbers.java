package pl.prime;

import java.util.*;

public class PrimeNumbers {

    public List<Integer> generatePrimeNumbers(int range) {
        List<Integer> list = new ArrayList<>();

        for (int i = 1; i <= range; i++)
        {
            if (isPrimeNumber(i)){
                list.add(i);
            }
        }
        return list;
    }

    private boolean isPrimeNumber(int i){
        int counter=0;
        for(int num =i; num>=1; num--)
        {
            if(i%num==0)
            {
                counter = counter + 1;
            }
        }
        if (counter ==2)
        {
            return true;
        }
        return false;
    }
}
